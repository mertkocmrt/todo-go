package main

import (
	"net/http"
	"sync"

	"github.com/gorilla/mux"
)

type ITodoRouter interface {
	InitRouter() *mux.Router
}

type router struct{}

func (router *router) InitRouter() *mux.Router {
	todoController := ServiceContainer().InjectTodoController()

	r := mux.NewRouter()
	http.Handle("/", r)
	r.HandleFunc("/list", todoController.GetTaskList).Methods("GET", "OPTIONS")
	r.HandleFunc("/add", todoController.AddNewTask).Methods("POST", "OPTIONS")

	return r
}

var (
	m          *router
	routerOnce sync.Once
)

func TodoRouter() ITodoRouter {
	if m == nil {
		routerOnce.Do(func() {
			m = &router{}
		})
	}
	return m
}
