package controllers

import (
	"encoding/json"
	"net/http"
	"todo/interfaces"
	"todo/models"
)

type TodoController struct {
	interfaces.ITodoService
}

func (controller *TodoController) GetTaskList(res http.ResponseWriter, req *http.Request) {

	tasks, err := controller.GetTasksService()
	if err != nil {
		panic(err)
	}

	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type,access-control-allow-origin, access-control-allow-headers")
	res.WriteHeader(http.StatusOK)

	resBody, _ := json.Marshal(tasks)
	res.Write(resBody)
}

func (controller *TodoController) AddNewTask(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type,access-control-allow-origin, access-control-allow-headers")
	res.WriteHeader(http.StatusOK)
	var t models.Task

	if req.ContentLength != 0 {
		err := json.NewDecoder(req.Body).Decode(&t)
		if err != nil {
			http.Error(res, err.Error(), http.StatusBadRequest)
			return
		}
	}

	task, err := controller.AddTaskService(t.Name)
	if err != nil {
		panic(err)
	}

	resBody, _ := json.Marshal(task)
	res.Write(resBody)
}
