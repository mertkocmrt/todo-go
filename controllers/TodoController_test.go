package controllers

import (
	"encoding/json"
	"net/http/httptest"
	"testing"
	"todo/interfaces/mocks"
	"todo/models"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestTodoForList(t *testing.T) {

	todoService := new(mocks.ITodoService)

	todo1 := models.Task{}
	todo1.ID = 1
	todo1.Name = "tada"

	todo2 := models.Task{}
	todo2.ID = 2
	todo2.Name = "todo"

	todoList := models.TaskCollection{}
	todoList.Tasks = append(todoList.Tasks, todo1, todo2)

	todoService.On("GetTaskList").Return(todoList, nil)

	todoController := TodoController{todoService}

	req := httptest.NewRequest("GET", "http://localhost:1234/list/", nil)
	w := httptest.NewRecorder()

	r := mux.NewRouter()
	r.HandleFunc("/list", todoController.GetTaskList)

	r.ServeHTTP(w, req)

	expectedResult := models.TaskCollection{}
	todoList.Tasks = append(todoList.Tasks, todo1, todo2)

	actualResult := models.TaskCollection{}

	json.NewDecoder(w.Body).Decode(&actualResult)

	assert.Equal(t, expectedResult, actualResult)
}
func TestTodoForNewTask(t *testing.T) {

	todoService := new(mocks.ITodoService)

	todo1 := models.Task{}

	todoService.On("AddNewTask", mock.Anything).Return(todo1, nil)

	todoController := TodoController{todoService}

	req := httptest.NewRequest("GET", "http://localhost:1234/add/", nil)

	w := httptest.NewRecorder()

	r := mux.NewRouter()
	r.HandleFunc("/add", todoController.AddNewTask)

	r.ServeHTTP(w, req)

	expectedResult := models.Task{}

	actualResult := models.Task{}

	json.NewDecoder(w.Body).Decode(&actualResult)

	assert.Equal(t, expectedResult, actualResult)
}
