package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"testing"
	"todo/models"

	"github.com/gorilla/mux"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"

	"github.com/pact-foundation/pact-go/utils"
)

var TaskList = &models.TaskCollection{}
var Task = &models.Task{}

// The Provider verification
func TestPactProvider(t *testing.T) {
	go startInstrumentedProvider()
	pact := setup()

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:    fmt.Sprintf("http://127.0.0.1:%d", port),
		FailIfNoPactsFound: false,
		PactURLs:           []string{filepath.FromSlash(fmt.Sprintf("%s/consumer-provider.json", "./pacts"))},
		StateHandlers:      stateHandlers,
		BeforeEach: func() error {
			TaskList = taskWHen
			Task = taskAddition
			return nil
		},
		AfterEach: func() error {
			TaskList = &models.TaskCollection{}
			Task = &models.Task{}
			return nil
		},
		ProviderVersion: "1.0.0",
	})

	if err != nil {
		t.Fatal(err)
	}
}

var stateHandlers = types.StateHandlers{
	"todo exist": func() error {
		TaskList = taskWHen
		return nil
	},
	"todo added": func() error {
		Task = taskAddition
		return nil
	},
}

func startInstrumentedProvider() {

	todoController := ServiceContainer().InjectTodoControllerTest()

	r := mux.NewRouter()

	r.HandleFunc("/add", todoController.AddNewTask).Methods("POST", "OPTIONS")
	r.HandleFunc("/list", todoController.GetTaskList).Methods("GET", "OPTIONS")
	http.Handle("/", r)

	http.ListenAndServe(fmt.Sprintf(":%d", port), r)

}

// Configuration / Test Data
var dir, _ = os.Getwd()
var pactDir = fmt.Sprintf("%s/../../pacts", dir)
var logDir = fmt.Sprintf("%s/log", dir)
var port, _ = utils.GetFreePort()

var taskWHen = &models.TaskCollection{
	Tasks: []models.Task{
		{
			ID:   1,
			Name: "effortless effort",
		},
	},
}
var taskAddition = &models.Task{
	ID:   1,
	Name: "effortless effort",
}

// Setup the Pact client.
func setup() dsl.Pact {
	return dsl.Pact{
		Provider:                 "provider",
		LogDir:                   logDir,
		PactDir:                  pactDir,
		DisableToolValidityCheck: true,
		LogLevel:                 "TRACE",
	}
}
