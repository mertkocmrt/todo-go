FROM golang:1.16-alpine

WORKDIR /app

ENV PATH=$PWD/pact/bin/:$PATH
ENV CGO_ENABLED=0 

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

COPY . ./
RUN go build -o /go-echo-vue

EXPOSE 1234

CMD [ "/go-echo-vue" ]