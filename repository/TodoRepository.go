package repository

import (
	"todo/interfaces"
	"todo/models"

	_ "github.com/mattn/go-sqlite3"
)

type TodoRepositoryWithCircuitBreaker struct {
	TodoRepository interfaces.ITodoRepository
}

type TodoRepository struct {
	interfaces.IDbHandler
}

func (repository *TodoRepository) GetTasksRepo() (*models.TaskCollection, error) {

	sql := "SELECT * FROM tasks"
	rows, err := repository.Query(sql)
	result := &models.TaskCollection{}
	// make sure to cleanup when the program exits
	defer rows.Close()

	if err != nil {
		return result, err
	}

	for rows.Next() {
		task := &models.Task{}
		err2 := rows.Scan(&task.ID, &task.Name)
		// Exit if we get an error
		if err2 != nil {
			return result, err
		}
		result.Tasks = append(result.Tasks, *task)
	}
	return result, nil
}

func (repository *TodoRepository) AddTaskRepo(name string) (*models.Task, error) {
	sql := "INSERT INTO tasks(name) VALUES(?)"

	stmt, err := repository.QueryWithPreparedStatement(sql)
	if err != nil {
		panic(err)
	}

	defer stmt.Close()

	result, err2 := stmt.Exec(name)
	if err2 != nil {
		panic(err2)
	}
	id, err3 := result.LastInsertId()
	return &models.Task{id, name}, err3

}
