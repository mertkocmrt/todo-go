package main

import (
	"net/http"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	http.ListenAndServe(":1234", TodoRouter().InitRouter())
}
