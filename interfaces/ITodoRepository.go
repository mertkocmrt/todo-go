package interfaces

import "todo/models"

type ITodoRepository interface {
	GetTasksRepo() (*models.TaskCollection, error)
	AddTaskRepo(name string) (*models.Task, error)
}
