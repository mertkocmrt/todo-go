package interfaces

import "todo/models"

type ITodoService interface {
	GetTasksService() (*models.TaskCollection, error)
	AddTaskService(name string) (*models.Task, error)
}
