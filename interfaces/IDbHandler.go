package interfaces

import "database/sql"

type IDbHandler interface {
	Execute(statement string)
	Query(statement string) (IRow, error)
	QueryWithPreparedStatement(statement string) (*sql.Stmt, error)
}

type IRow interface {
	Scan(dest ...interface{}) error
	Next() bool
	Close() error
}
