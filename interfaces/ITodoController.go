package interfaces

import (
	"net/http"
)

type ITodoController interface {
	GetTaskList(res http.ResponseWriter, req *http.Request)
	AddNewTask(res http.ResponseWriter, req *http.Request)
}
