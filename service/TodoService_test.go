package service

import (
	"testing"
	"todo/interfaces/mocks"
	"todo/models"

	"github.com/stretchr/testify/assert"
)

func TestGetTasksService(t *testing.T) {

	todoRepository := new(mocks.ITodoRepository)

	todo1 := models.Task{}
	todo1.ID = 1
	todo1.Name = "tada"

	todo2 := models.Task{}
	todo2.ID = 2
	todo2.Name = "todo"

	todoList := models.TaskCollection{}
	todoList.Tasks = append(todoList.Tasks, todo1, todo2)

	todoRepository.On("GetTasksRepo").Return(&todoList, nil)

	todoService := TodoService{todoRepository}

	expectedResult := &todoList

	actualResult, _ := todoService.GetTasksService()

	assert.Equal(t, expectedResult, actualResult)
}

func TestAddTaskService(t *testing.T) {

	todoRepository := new(mocks.ITodoRepository)

	todo1 := models.Task{}
	todo1.ID = 1
	todo1.Name = "tada"

	todoRepository.On("AddTaskRepo", "tada").Return(&todo1, nil)

	todoService := TodoService{todoRepository}

	expectedResult := &todo1

	actualResult, _ := todoService.AddTaskService("tada")

	assert.Equal(t, expectedResult, actualResult)
}
