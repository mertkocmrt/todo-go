package service

import (
	"todo/interfaces"
	"todo/models"
)

type TodoService struct {
	interfaces.ITodoRepository
}

func (service *TodoService) GetTasksService() (*models.TaskCollection, error) {

	tasks, err := service.GetTasksRepo()

	if err != nil {
		panic(err)
	}

	return tasks, nil
}

func (service *TodoService) AddTaskService(name string) (*models.Task, error) {

	task, err := service.AddTaskRepo(name)

	if err != nil {
		panic(err)
	}

	return task, nil
}
