# TODO APP

TODO List is a todo list application implemented by VUE.JS and GOLANG. This project is server-side of the TODO List.

This application was developed for demo purpose.

- Sqlite3 for light database
- Golang for provider 
- Pact for CDC test
- Jest for unit test

## Features
- Rest API for adding and fetching TODO items

## Installation

Follow the Vue Js Guide for getting started building a project.
Clone or download the repo
go run todo.go to start back-end at localhost:1234

or

run docker image created 
by 
docker build --tag go-todo . 
then 
docker run go-todoapp

# Unit Test
Jest is used as unit and component test framework. 

go test

# CDC Test
Pact is used as a CDC Test framework. 

go test
also runs provider test and it reads consumer-provider.json locally verify contract.

# GITLAB CI/CD
After push to remote repository gitlab job starts.

