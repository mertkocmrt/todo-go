package main

import (
	"database/sql"
	"sync"
	"todo/controllers"
	"todo/infrastructures"
	"todo/repository"
	"todo/service"
)

type kernel struct{}

type IServiceContainer interface {
	InjectTodoController() controllers.TodoController
	InjectTodoControllerTest() controllers.TodoController
}

func (k *kernel) InjectTodoController() controllers.TodoController {

	sqlConn, _ := sql.Open("sqlite3", "storage.db")
	createStatement := `
    CREATE TABLE IF NOT EXISTS tasks(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        name VARCHAR NOT NULL
    );
    `
	sqliteHandler := &infrastructures.SQLiteHandler{}
	sqliteHandler.Conn = sqlConn

	sqliteHandler.Conn.Exec(createStatement)

	todoRepository := &repository.TodoRepository{sqliteHandler}
	todoService := &service.TodoService{todoRepository}
	todoController := controllers.TodoController{todoService}

	return todoController
}

func (k *kernel) InjectTodoControllerTest() controllers.TodoController {

	sqlConn, _ := sql.Open("sqlite3", "test.db")
	createStatement := `
    	CREATE TABLE IF NOT EXISTS tasks(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        name VARCHAR NOT NULL
    );
    `
	dropStatement := `
		DROP TABLE tasks;
	`
	sqliteHandler := &infrastructures.SQLiteHandler{}
	sqliteHandler.Conn = sqlConn

	sqliteHandler.Conn.Exec(dropStatement)

	sqliteHandler.Conn.Exec(createStatement)

	todoRepository := &repository.TodoRepository{sqliteHandler}
	todoService := &service.TodoService{todoRepository}
	todoController := controllers.TodoController{todoService}

	return todoController
}

var (
	k             *kernel
	containerOnce sync.Once
)

func ServiceContainer() IServiceContainer {
	if k == nil {
		containerOnce.Do(func() {
			k = &kernel{}
		})
	}
	return k
}
