package infrastructures

import (
	"database/sql"
	"todo/interfaces"
)

type SQLiteHandler struct {
	Conn *sql.DB
}

func (handler *SQLiteHandler) Execute(statement string) {
	handler.Conn.Exec(statement)
}

func (handler *SQLiteHandler) QueryWithPreparedStatement(statement string) (*sql.Stmt, error) {

	stmt, err := handler.Conn.Prepare(statement)

	if err != nil {
		panic(err)
	}

	return stmt, nil
}

func (handler *SQLiteHandler) Query(statement string) (interfaces.IRow, error) {
	rows, err := handler.Conn.Query(statement)
	if err != nil {
		panic(err)
	}
	return rows, nil
}

type SqliteRow struct {
	Rows *sql.Rows
}

func (r SqliteRow) Scan(dest ...interface{}) error {
	err := r.Rows.Scan(dest...)
	if err != nil {
		return err
	}

	return nil
}

func (r SqliteRow) Next() bool {
	return r.Rows.Next()
}

func (handler *SQLiteHandler) Close() error {
	return handler.Conn.Close()
}
