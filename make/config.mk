SHELL = "/bin/bash"

export PATH := $(PWD)/pact/bin:$(PATH)
export PATH
export PROVIDER_NAME = provider
export CONSUMER_NAME = consumer
export PACT_DIR = $(PWD)/pacts
export LOG_DIR = $(PWD)/log